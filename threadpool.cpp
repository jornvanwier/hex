#include <thread>
#include <vector>
#include <atomic>

#include "threadpool.h"
#include "worker.h"

ThreadPool::ThreadPool()
{
    std::cout << "Creating threadpool." << std::endl;
    stop = false;
    taskCounter = 0;
    for(unsigned i = 0; i < std::thread::hardware_concurrency(); i++)
    {
        workers.push_back(std::thread(Worker(*this)));
    }
}

ThreadPool::~ThreadPool()
{
    stop = true;
    cond.notify_all();
    
    for(auto &thread: workers)
        thread.join();
}

//template<class F>
void ThreadPool::Enqueue(std::function<MoveEval()> task)
{
    std::unique_lock<std::mutex> locker(mutex);
    tasks.push_back(task);
    ++taskCounter;
    locker.unlock();
    cond.notify_one();
}

std::vector<MoveEval> ThreadPool::GetBestMoves()
{
    std::unique_lock<std::mutex> locker(mutex);
    allDone.wait(locker);

    std::vector<MoveEval> temp;

    bestMoves.swap(temp);

    return temp;    
}
