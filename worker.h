#include "threadpool.h"

using MoveEval = std::pair<Move,int>;

class Worker {
public:
    Worker(ThreadPool &s): pool(s) { }
    void operator()();
private:
    ThreadPool &pool;
};

