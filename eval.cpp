// eval.cpp
// Aswin van Woudenberg

#include "eval.h"
#include "move.h"

#include "math.h"
#include <tuple>
#include <limits>
#include <algorithm>
#include <map>

int getMonteCarloEval(const State &board, Player player, Player opponent, int trials)
{
    std::vector<Move> moves = board.getMoves();

    int wins = 0;

    for (int i = 0; i < trials; ++i)
    {
        State mcBoard = board;

        std::random_shuffle(moves.begin(), moves.end());
        for (const Move &m: moves) 
            mcBoard.doMove(m);        
        if (mcBoard.getWinner() == player) 
            wins++;
    }
    
    return wins - trials / 2;
}

//int getRandomEval(const State &board, Player player)
//{
//    return rand() % 101 - 50;
//}

int StartElectricEval(const State &board, const Player &player, const Player &opponent)
{ 
    int boardSize = board.getSize();

    long blueResistance = getElectricEval(board, boardSize * boardSize + 2, boardSize * boardSize + 3, player, opponent); 
    long redResistance = getElectricEval(board, boardSize * boardSize, boardSize * boardSize + 1, player, opponent); 

    if(player == Player::RED)
    {
        if(redResistance == 0) return infinite;
        if(redResistance > infinite) return 0;
        if(blueResistance == 0) return 0;
    }
    else if(player == Player::BLUE)
    {
        if(blueResistance == 0) return infinite;
        if(blueResistance > infinite) return 0;
        if(redResistance == 0) return 0;
    }

    return player == Player::RED ? -redResistance / blueResistance : -blueResistance / redResistance;  
}

long getElectricEval(const State &board, const int &startNode, const int &endNode, const Player &player, const Player &opponent)
{
    std::map<int, int> nodesToEval;
    std::vector<int> evaluatedNodes;
    
    std::pair<int, int> currentNode;
    nodesToEval[startNode] = getResistance(board, startNode, player, opponent);    

    long totalResistance = 0;

    while(true)
    {
        //get lowest resistance;
        auto it = std::min_element(nodesToEval.begin(), nodesToEval.end(), [] (const std::pair<int, int> &n1, const std::pair<int, int> &n2) { return n1.second < n2.second; });
        currentNode = *it;
        nodesToEval.erase(it);
        evaluatedNodes.push_back(currentNode.first);  
            
        totalResistance += currentNode.second;    

        //return total resistance if the endnode has been reached
        if(currentNode.first == endNode) return totalResistance;

        std::vector<int> adjacentHexs = board.getAllAdjacentHexagons(currentNode.first);

        int nearbyFriendlyHexs = 0;
        
        for(int &node: adjacentHexs)
        {            
            //skip if node has been evaluated already
            if(std::find(evaluatedNodes.begin(), evaluatedNodes.end(), node) != evaluatedNodes.end()) continue;
           
            if(board.hexagons[node] == player) nearbyFriendlyHexs++;

            //add node to open if it it doesnt exist
            if(nodesToEval.count(node) == 0) 
            {
                int resistance = getResistance(board, node, player, opponent);
                
                nodesToEval[node] = resistance;
            }
        }

        if(nearbyFriendlyHexs > 1) totalResistance--;
    }
}

int getResistance(const State &board, const int &hex, const Player &player, const Player &opponent)
{
    if(board.hexagons[hex] == player) return 0;
    if(board.hexagons[hex] == opponent) return std::numeric_limits<int>::max();
    else return 1;
}
