// eval.h
// Aswin van Woudenberg

#include "state.h"

#ifndef EVAL_H
#define EVAL_H

const int infinite = std::numeric_limits<int>::max();

int getMonteCarloEval(const State &board, Player player, Player opponent, int trials);
int getRandomEval(const State &board, Player player);
int StartElectricEval(const State &board, const Player &player, const Player &opponent);
long getElectricEval(const State &board, const int &startNode, const int &endNode, const Player &player, const Player &opponent);
int getResistance(const State &board, const int &hex, const Player &player, const Player &opponent);
#endif // EVAL_H

