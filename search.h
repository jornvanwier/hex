// search.h
// Aswin van Woudenberg

#include <utility>
#include <functional>

#include "move.h"
#include "state.h"

#ifndef SEARCH_H
#define SEARCH_H

#include "threadpool.h"

using MoveEval = std::pair<Move,int>;

//Move alphaBeta(const State &board, int ply, std::function<int(State &state, Player p)> eval);
MoveEval alphaBeta(State *board, int ply, Player player, Player opponent, int alpha, int beta, std::function<int(State &state, const Player p, const Player o)> eval);
Move StartThreadedAlphaBeta(ThreadPool &pool, const State &b, int ply, std::function<int(State &state, const Player p, const Player o)> eval);

#endif // SEARCH_H
