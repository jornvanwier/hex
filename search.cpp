// search.cpp
// Aswin van Woudenberg

#include <vector>
#include <deque>
#include <limits>
#include <functional>
#include <iostream>
#include <random>
#include <algorithm>
#include <map>

#include "search.h"
#include "eval.h"
#include "threadpool.h"
#include "game.h"

int maxPlies;

MoveEval alphaBeta(State *board, int ply, Player player, Player opponent, int alpha, int beta, std::function<int(State &state, const Player p, const Player o)> eval)
{

    std::vector<Move> moves = board->getRelevantMoves();

    if (moves.size() == 0)
    {
        std::cout << "Getting random move." << std::endl;
       
        int size = board->getSize() - 1;

        std::random_device rd;
        std::mt19937 generator(rd());
        std::uniform_int_distribution<int> dist(0, size);

        int count = 0;

        Move move;
        //do random move
        do
        {
            if(count > 20) return(std::make_pair(Move(), -alpha));
            move = std::make_pair(dist(generator), dist(generator));
            std::cout << "generated move: " << move.first << "," << move.second << std::endl;
            std::cout << "player at location: " << board->hexagons[move.first * size + move.second] << std::endl;
            count++;
        } while (!board->isEmptyHexagon(move));
        return std::make_pair(move, alpha);
    }
    
    if (ply == 0) return std::make_pair(moves[0], eval(*board, player, opponent));

    bool first = true;
    MoveEval best;

    for (Move &move: moves) {
        board->doMove(move, player);
        MoveEval me = alphaBeta(board, ply - 1, opponent, player, -beta, -alpha, eval);
        board->undoMove(move, player);

        if(first) 
        {
            best = me;
            first = false;
        }
        else
        {
            if (-me.second > alpha) {
                alpha = -me.second;
                best = std::make_pair(move,alpha);
            }
            if (alpha >= beta) 
            {
                if(ply == maxPlies)
                    delete board;
                
                return best;
            }
        }
    }
    if(ply == maxPlies)
        delete board;

    return best;
}

Move StartThreadedAlphaBeta(ThreadPool &pool, const State &b, int ply, std::function<int(State &state, const Player p, const Player o)> eval)
{
    Player player = b.getPlayer();
    Player opponent = b.getOpponent(); 
    
    maxPlies = ply;
    
    std::vector<Move> moves = b.getRelevantMoves();

    std::cout << "Found relevant moves: " << moves.size() << std::endl;    

    if (moves.size() == 0)
    {
        std::cout << "Getting random move." << std::endl;
            
        int size = b.getSize() - 1;    
               
        int count = 0;

        std::random_device rd;
        std::mt19937 generator(rd());
        std::uniform_int_distribution<int> dist(0, size);

        Move move;
        //do random move
        do
        {
            move = std::make_pair(dist(generator), dist(generator));
            std::cout << "generated move: " << move.first << "," << move.second << std::endl;
            count++;
        } while (!b.isEmptyHexagon(move));
        return move;
    }
       
    int alpha = std::numeric_limits<int>::min() + 1;
    int beta = std::numeric_limits<int>::max();

    for(Move move: moves)
    {
        std::cout << "Creating new task." << std::endl;
        State *boardCopy = new State(b);
        boardCopy->doMove(move, player);
        pool.Enqueue([boardCopy, ply, opponent, player, alpha, beta, eval] { return alphaBeta(boardCopy, ply - 1, opponent, player, alpha, beta, eval); } );
    }
    
    std::cout << "All tasks scheduled." << std::endl;
    
    std::vector<MoveEval> bestMoves = pool.GetBestMoves();
 
    std::cout << "Found best moves: " << bestMoves.size() << std::endl;

    bool first = true;
    MoveEval best;
    for (MoveEval &me: bestMoves) {
        if(first)
        {
            best = me;
            first = false;
        }
        else
        {
            if (-me.second > alpha) {
                alpha = -me.second;
                best = std::make_pair(me.first,alpha);
            }
            if (alpha >= beta) 
            {
                std::cout << "moves contains best move: " << (std::find(moves.begin(), moves.end(), best.first) != moves.end()) << std::endl;
                std::cout << "bestmoves contains best move: " << (std::find(bestMoves.begin(), bestMoves.end(), best) != bestMoves.end()) << std::endl;
                return best.first;
            }
        }
    }

    std::cout << "moves contains best move: " << (std::find(moves.begin(), moves.end(), best.first) != moves.end()) << std::endl;
    std::cout << "bestmoves contains best move: " << (std::find(bestMoves.begin(), bestMoves.end(), best) != bestMoves.end()) << std::endl;

    return best.first;
}
