#include<thread>
#include <mutex>
#include <condition_variable>

#include "threadpool.h"
#include "worker.h"

void Worker::operator()() 
{

    std::function<MoveEval()> task;

    while (true)
    {
        std::unique_lock<std::mutex> locker(pool.mutex);

        pool.cond.wait(locker, [&](){ return !pool.tasks.empty() || pool.stop; });
        if(pool.stop) return;

        task = pool.tasks.front();
        pool.tasks.pop_front();
        locker.unlock();
        
        MoveEval move = task();

        locker.lock();
        pool.bestMoves.push_back(move);
        
        std::cout << "Done with task. Remaining: " << pool.taskCounter << std::endl;

        pool.taskCounter--;

        if(pool.taskCounter == 0) pool.allDone.notify_all(); 
        
    }

}
