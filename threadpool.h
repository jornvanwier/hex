#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <thread>
#include <vector>
#include <deque>
#include <mutex>
#include <condition_variable>


#include "move.h"

using MoveEval = std::pair<Move,int>;

class ThreadPool 
{
    friend class Worker;
public:
    ThreadPool();
    ~ThreadPool();
    std::deque<std::function<MoveEval()>> tasks;
    /*template<class F>*/ void Enqueue(std::function<MoveEval()> task);
    std::condition_variable cond;
    std::condition_variable allDone;
    std::mutex mutex;
    std::vector<MoveEval> GetBestMoves();
private:
    std::vector<std::thread> workers;
    void WorkerAwaitTask(); 

protected:
    bool stop;
    std::vector<MoveEval> bestMoves;
    int taskCounter;
};

#endif
